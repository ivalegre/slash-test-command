module.exports = function(Command) {
  Command.moza = function(cb) {
    var jsdom = require("jsdom");

    jsdom.env(
      "http://yonkiblog.com/category/mozas-2/mozas/",
      ["http://code.jquery.com/jquery.js"],
      function (err, window) {
        if (err){
          console.log(err);
        } else {
          console.log("Successfully downloaded Yonkiblog mozas page");
        }
        var $ = window.$;
        var image_url = $(".entry-content img")[0].src;
        var response = {
          'response_type': 'in_channel',
          // 'text': 'Aquí está tu moza, pirata',
          text: "Aquí está tu moza, piratón:\n<" + $(".entry-title a")[0].href + "|" + $(".entry-title a")[0].text + ">",
          'attachments': [{
              image_url: image_url,
              thumb_url: image_url,
              author_name: 'Yonkiblog'
          }]
        };

        cb(null, response);
      }
    );
  };
  Command.remoteMethod(
    'moza',
    {
      http: {path: '/moza', verb: 'get'},
      returns: {root: true, type: 'object'}
    }
  );
};
